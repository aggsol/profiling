/**
 * @brief Profiling header file
 * @author Wolfram Rösler <wolfram@roesler-ac.de>
 * @date 2018-07-07
 */

#pragma once

#include <chrono>

namespace profiler {
    /**
     * Profiler class.
     *
     * Usage: To measure the duration spent in a code block, put the following at
     * the beginning of the block:
     *
     *      PROFILE(name);
     *
     * where name is a string literal that uniquely identifies the block. For example:
     *
     *      PROFILE("Computation loop");
     *
     * The output file (profiler.out in the current directory) is created when
     * the program ends. No output is created when the program `abort()`s, e. g.
     * because of an uncaught exception.
     */
    class Profiler {
    public:
        const char* const name = nullptr;
        const std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

        explicit Profiler(const char* n) : name(n) {}
        ~Profiler();
    };
}

#define PROFILE(x) profiler::Profiler profiler_object(x)
