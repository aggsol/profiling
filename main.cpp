/**
 * @brief Profiling example program
 * @author Wolfram Rösler <wolfram@roesler-ac.de>
 * @date 2018-07-07
 */

#include <chrono>
#include <thread>
#include "profiler.h"

using namespace std::chrono_literals;

int main() {

    // Something that takes one second
    {
        PROFILE("sleep 1 s");
        std::this_thread::sleep_for(1s);
    }

    // Something that takes 1/10 second
    for(auto _=0;_<10;++_) {
        PROFILE("sleep 100 ms");
        std::this_thread::sleep_for(100ms);
    }

    // Something that takes 1/1000 second
    for(auto _=0;_<1000;++_) {
        PROFILE("sleep 1 ms");
        std::this_thread::sleep_for(1ms);
    }
}
